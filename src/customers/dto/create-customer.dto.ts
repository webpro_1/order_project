import {
  IsNumber,
  IsNotEmpty,
  IsString,
  IsPositive,
  Length,
} from 'class-validator';

export class CreateCustomerDto {
  @IsNumber()
  @IsNotEmpty()
  @IsString()
  @IsPositive()
  age: number;
  @IsNotEmpty()
  @IsString()
  @Length(5, 50)
  name: string;
  @IsNotEmpty()
  @IsString()
  @Length(10)
  tel: string;
  @IsNotEmpty()
  @IsString()
  gender: string;
}
