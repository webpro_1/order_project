import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from './entities/customer.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CustomersService {
  constructor(
    @InjectRepository(Customer)
    private customerRepository: Repository<Customer>,
  ) {}
  create(createCustomerDto: CreateCustomerDto) {
    return this.customerRepository.save(createCustomerDto);
  }

  findAll() {
    return this.customerRepository.find({ relations: ['orders'] });
  }

  async findOne(id: number) {
    const user = await this.customerRepository.findOne({
      where: { id: id },
      relations: ['orders'],
    });
    if (!user) {
      throw new NotFoundException();
    }
    return user;
  }

  async update(id: number, updateCustomerDto: UpdateCustomerDto) {
    const user = await this.customerRepository.findOneBy({ id: id });
    const updateCustomer = {
      ...user,
      ...updateCustomerDto,
    };
    return this.customerRepository.save(updateCustomer);
  }

  async remove(id: number) {
    const user = await this.customerRepository.findOneBy({ id: id });
    return this.customerRepository.softRemove(user);
  }
}
