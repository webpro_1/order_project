import { Type } from 'class-transformer';
import {
  IsNotEmpty,
  IsArray,
  IsNumber,
  IsPositive,
  Min,
  ValidateNested,
} from 'class-validator';

export class CreateOrderItemDto {
  @IsNumber()
  @IsNotEmpty()
  @Min(1)
  productId: number;

  @IsNotEmpty()
  @Min(1)
  @IsNumber()
  amount: number;
}
export class CreateOrderDto {
  @IsNotEmpty()
  @IsPositive()
  @IsNumber()
  @Min(1)
  customerId: number;
  @IsNotEmpty()
  @IsArray()
  @ValidateNested()
  @Type(() => CreateOrderItemDto)
  orderItems: CreateOrderItemDto[];
}
