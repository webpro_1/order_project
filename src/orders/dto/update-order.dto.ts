import { PartialType } from '@nestjs/mapped-types';
import { CreateOrderItemDto } from './create-order.dto';
import { CreateOrderDto } from './create-order.dto';

export class UpdateOrderDto extends PartialType(CreateOrderDto) {}
export class UpdateOrderItemDto extends PartialType(CreateOrderItemDto) {}
